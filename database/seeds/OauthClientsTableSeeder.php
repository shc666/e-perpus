<?php

use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oauth_clients')->delete();
        
        \DB::table('oauth_clients')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => NULL,
                'name' => 'Granth Personal Access Client',
                'secret' => 'VKJAKmy36UDWr5pYn1XPcJi3M0oXUMDtviodDpOu',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0,
                'created_at' => '2020-04-23 05:41:36',
                'updated_at' => '2020-04-23 05:41:36',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => NULL,
                'name' => 'Granth Password Grant Client',
                'secret' => 'uPwVnePtyZOhGk4A1VctxBZuijwzGld5IPn32Kwh',
                'redirect' => 'http://localhost',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
                'created_at' => '2020-04-23 05:41:36',
                'updated_at' => '2020-04-23 05:41:36',
            ),
        ));
        
        
    }
}