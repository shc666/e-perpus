<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;


// /* Create Syombolic Link Storage Callback */
// Route::get('/storage-link', function () {
//     Artisan::call('storage:link');
// });

/* Auth Verify */
Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth', 'verified', 'xss']], function()
{
    Route::get('/', 'HomeController@index');
    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::view('/test', 'auth.verify');
});

/* Admin Routes */
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['auth','xss']], function () {

    /* Category Routes */
    Route::resource('category', 'CategoryController');
    Route::get('category/edit/{id}', 'CategoryController@create')->name('category.edit');
    Route::get('category-list', 'CategoryController@list')->name('category.list');
    Route::delete('category/destroy/{id}', 'CategoryController@destroy')->name('category.destroy');

    /* Subcategory Routes */
    Route::get('subcategory/dropdown', 'SubCategoryController@getsubCategoryList')->name('subcategory.dropdown');
    Route::resource('subcategory','SubCategoryController');
    Route::get('subcategory-list', 'SubCategoryController@list')->name('subcategory.list');
    Route::get('subcategory/edit/{id}', 'SubCategoryController@create')->name('subcategory.edit');
    Route::delete('subcategory/destroy/{id}', 'SubCategoryController@destroy')->name('subcategory.destroy');

    /* Static Data */
    Route::resource('static-data', 'StaticDataController');
    Route::get('static-data/edit/{id}', 'StaticDataController@create')->name('static-data.edit');
    Route::get('static-data-list', 'StaticDataController@list')->name('static-data.list');
    Route::delete('static-data/destroy/{id}', 'StaticDataController@destroy')->name('static-data.destroy');

    /* Author Routes */
    Route::resource('author','AuthorController');
    Route::get('author-list/{type?}','AuthorController@dataList')->name('dataList');
    Route::get('author-list/edit/{id}','AuthorController@create')->name('author.edit');
    Route::get('author-view/{id}','AuthorController@show')->name('author.show');
    Route::delete('author/destroy/{id}','AuthorController@destroy')->name('author.destroy');

    /* Mobile Slider */
    Route::resource('mobileslider', 'MobileSliderController');
    Route::get('mobileslider-list', 'MobileSliderController@list')->name('mobileslider.list');
    Route::get('mobileslider/edit/{id}', 'MobileSliderController@create')->name('mobileslider.edit');
    Route::get('mobileslider/destroy/{id}', 'MobileSliderController@destroy')->name('mobileslider.destroy');

    /* Book Routes */
    Route::resource('book', 'BookController');
    Route::get('book-edit/{id?}','BookController@create')->name('book.update');
    Route::get('book-list/{type?}','BookController@bookList')->name('book.list');
    Route::get('book-view/{id}','BookController@view')->name('book.view');
    Route::get('book-destroy/{id}','BookController@destroy')->name('book.delete');
    Route::post('book-action','BookController@bookActions')->name('book.actions');

    /* Setting Routes */
    Route::get('privacy-policy','SettingController@privacyPolicy')->name('privacy-policy');
    Route::post('privacy-policy-save','SettingController@savePrivacyPolicy')->name('privacy-policy-save');
    Route::get('term-condition','SettingController@termAndCondition')->name('term-condition');
    Route::post('term-condition-save','SettingController@saveTermAndCondition')->name('term-condition-save');

    /* Feedback Routes */
    Route::get('users/feedback','UsersController@userFeedback')->name('users_feedback');
    Route::get('users/feedback/datalist','UsersController@userFeedbackDataList')->name('users_feedback.list');

    /* Sales Routes */
    Route::get('/transactions/list/{id?}/{record?}','TransactionController@list')->name('transactions.list');
    Route::resource('transactions','TransactionController');
    Route::get('update-payment-status/{id}/{status}','TransactionController@updatePaymentStatus')->name('transactions_update.payment_status');

    /* User Details Routes */
    Route::resource('users','UsersController');
    Route::get('user-list','UsersController@list')->name('user.list');
    Route::get('user-delete/{id}','UsersController@destroy')->name('user.delete');
    Route::post('/password/upadte', 'UsersController@passwordUpdate')->name('user.password.update');
    Route::post('/profile/save', 'UsersController@userUpdate')->name('user.update');

    /* Settings Routes */
    Route::get('settings', 'SettingController@settings')->name('admin.settings');
    Route::post('/layout-page', 'SettingController@layoutPage')->name('layout_page');
    Route::post('settings/save', 'SettingController@settingsUpdates')->name('settingsUpdates');
    Route::post('contact-us', 'SettingController@contactus_settings')->name('contactus_settings');
    Route::post('env-setting', 'SettingController@envSetting')->name('envSetting');
    Route::get('mobile-app','SettingController@getMobileSetting')->name('mobile_app.config');
    Route::post('mobile-app/save','SettingController@saveMobileSetting')->name('mobile_app.config.save');
});