<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'    => 'Username atau password tidak cocok. Silahkan mencoba kembali.',
    'throttle'  => 'Terlalu banyak percobaan untuk masuk. Silahkan ulangi kembali dalam :seconds detik.',
];