@extends('layouts.master')

@section('content')
@php
    $auth_user = authSession();
@endphp

<link href="{{ asset('/plugin/tooltip/tooltip-flip.css') }}" rel="stylesheet" type="text/css" />
    <!-- Card stats -->
    <div class="row">
        @if($auth_user->is('admin'))
        <div class="col-xl-4 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total Buku</h5>
                            <span class="h2 font-weight-bold mb-0">{{ $data['card']['total_book'] }}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                                <i class="fas fa-book"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total Pengarang</h5>
                            <span class="h2 font-weight-bold mb-0">{{ $data['card']['total_author'] }}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-dark text-white rounded-circle shadow">
                                <i class="fas fa-user-edit"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total Pengguna</h5>
                            <span class="h2 font-weight-bold mb-0">{{ $data['card']['total_user'] }}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                                <i class="fas fa-user-alt"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @elseif($auth_user->is('sub_admin'))
        <div class="col-xl-6 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total Buku</h5>
                            <span class="h2 font-weight-bold mb-0">{{ $data['card']['total_book'] }}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                                <i class="fas fa-book"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total Pengarang</h5>
                            <span class="h2 font-weight-bold mb-0">{{ $data['card']['total_author'] }}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-dark text-white rounded-circle shadow">
                                <i class="fas fa-user-edit"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    <div class="row mt-4">
        @if($auth_user->is('admin'))
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-uppercase text-muted ls-1 mb-1">Pengguna</h6>
                            <h2 class="mb-0">Pengguna Baru</h2>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <!-- Chart -->
                    <div class="chart">
                        <canvas id="chart-users" class="chart-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    @if($auth_user->is('admin'))
    <div class="row mt-5" id="dashboard_tables">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Update Buku Terakhir</h3>
                        </div>
                        <div class="col text-right">
                            <a href="{{ route('book.index') }}" class="btn btn-sm btn-primary">Lihat Semua</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 table-responsive-lg">
                            <!-- Projects table -->
                            <table class="table align-items-center dataTable md-responsive table-responsive">
                                <thead class="thead-light">
                                <tr>
                                    <th width="1%">No</th>
                                    <th width="10%">Pengarang</th>
                                    <th width="10%">Buku</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($data['list']['book'])>0)
                                    @foreach($data['list']['book'] as $index=>$book)
                                        <tr>
                                            <th scope="row">
                                                {{ $index + 1 }}
                                            </th>
                                            <td>
                                                <a href="{{ route('author.show', ['id'=>$book->author_id, 'redirect_url'=>route('home')]) }}" class="tooltip tooltip-effect-3">
                                                    <b>{{ (optional($book->getAuthor)->name) }}</b>
                                                    <span class="tooltip-content">
                                                        <span class="tooltip-front">
                                                            <img class="m-inherit mlr-auto mt-6px" src="{{ getSingleMedia($book->getAuthor, 'image', null) }}" alt="user3"/>
                                                        </span>
                                                        <span class="tooltip-back">
                                                            <div>{{ (optional($book->getAuthor)->name) }}</div>
                                                        </span>
                                                    </span>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{route('book.view', ['id'=>$book->book_id, 'redirect_url'=>route('home')])}}">{{ optional($book)->name }}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <th colspan="4" scope="row" class="text-center">
                                            Tidak ada data ditemukan.
                                        </th>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="row mt-4" id="dashboard_tables">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Update Buku Terakhir</h3>
                        </div>
                        <div class="col text-right">
                            <a href="{{ route('book.index') }}" class="btn btn-sm btn-primary">Lihat Semua</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 table-responsive-lg">
                            <!-- Projects table -->
                            <table class="table align-items-center dataTable md-responsive table-responsive">
                                <thead class="thead-light">
                                    <tr>
                                        <th width="1%">No</th>
                                        <th width="10%">Pengarang</th>
                                        <th width="10%">Buku</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($data['list']['book'])>0)
                                    @foreach($data['list']['book'] as $index=>$book)
                                        <tr>
                                            <th scope="row">
                                                {{ $index + 1 }}
                                            </th>
                                            <td>
                                                <a  href="{{ route('author.show',['id'=>$book->author_id,'redirect_url'=>route('home')]) }}"  class="tooltip tooltip-effect-3" ><b>{{ (optional($book->getAuthor)->name) }}</b><span class="tooltip-content"><span class="tooltip-front"><img class="m-inherit mlr-auto mt-6px" src="{{ getSingleMedia($book->getAuthor,'image',null) }}" alt="user3"/></span><span class="tooltip-back"><div>{{ (optional($book->getAuthor)->name) }}</div></span></span></a>
                                            </td>
                                            <td>
                                                <a href="{{route('book.view',['id'=>$book->book_id,'redirect_url'=>route('home')])}}">{{ optional($book)->name }}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <th colspan="4" scope="row" class="text-center">
                                            Tidak ada data ditemukan.
                                        </th>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('body_bottom')
    <script>
        var chart_sales = "{{ implode(',',$data['graph']['sales']) }}";
        var chart_users = "{{ implode(',',$data['graph']['users']) }}";
        chart_sales = chart_sales.split(",");
        chart_users = chart_users.split(",");

        $(document).ready(function () {
            //
            // Sales chart
            //

            var SalesChart = (function() {

                // Variables

                var $chart = $('#chart-sales');


                // Methods

                function init($chart) {

                    var salesChart = new Chart($chart, {
                        type: 'line',
                        options: {
                            scales: {
                                yAxes: [{
                                    gridLines: {
                                        color: Charts.colors.gray[900],
                                        zeroLineColor: Charts.colors.gray[900]
                                    },
                                    ticks: {
                                        callback: function(value) {
                                            if (!(value % 10)) {
                                                return '$' + value;
                                            }
                                        }
                                    }
                                }]
                            },
                            tooltips: {
                                callbacks: {
                                    label: function(item, data) {
                                        var label = data.datasets[item.datasetIndex].label || '';
                                        var yLabel = item.yLabel;
                                        var content = '';

                                        if (data.datasets.length > 1) {
                                            content += '<span class="popover-body-label mr-auto">' + label + '</span>';
                                        }

                                        content += '<span class="popover-body-value">$' + yLabel + '</span>';
                                        return content;
                                    }
                                }
                            }
                        },
                        data: {
                            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"],
                            datasets: [{
                                label: 'Performance',
                                data: chart_sales
                            }]
                        }
                    });

                    // Save to jQuery object

                    $chart.data('chart', salesChart);

                };


                // Events

                if ($chart.length) {
                    init($chart);
                }

            })();

            //
            // Orders chart
            //

            var UsersChart = (function() {

                //
                // Variables
                //

                var $chart = $('#chart-users');
                var $usersSelect = $('[name="usersSelect"]');


                //
                // Methods
                //

                // Init chart
                function initChart($chart) {

                    // Create chart
                    var usersChart = new Chart($chart, {
                        type: 'bar',
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        callback: function(value) {
                                            if (!(value % 10)) {
                                                //return '$' + value + 'k'
                                                return value
                                            }
                                        }
                                    }
                                }]
                            },
                            tooltips: {
                                callbacks: {
                                    label: function(item, data) {
                                        var label = data.datasets[item.datasetIndex].label || '';
                                        var yLabel = item.yLabel;
                                        var content = '';

                                        if (data.datasets.length > 1) {
                                            content += '<span class="popover-body-label mr-auto">' + label + '</span>';
                                        }

                                        content += '<span class="popover-body-value">' + yLabel + '</span>';

                                        return content;
                                    }
                                }
                            }
                        },
                        data: {
                            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"],
                            datasets: [{
                                label: 'Sales',
                                data: chart_users
                            }]
                        }
                    });

                    // Save to jQuery object
                    $chart.data('chart', usersChart);
                }


                // Init chart
                if ($chart.length) {
                    initChart($chart);
                }

            })();

        });
    </script>
@endsection
