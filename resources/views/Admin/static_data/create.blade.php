@extends('layouts.master')

@section('content')
<div class="col-md-6 offset-md-3">
    <div class="card">
        <div class="card-header">
            <h3 class="d-inline">{{ isset($pageTitle) ? $pageTitle : trans('messages.static_data') }}</h3>
            <a href="{{ route('static-data.index') }}" class="btn btn-sm btn-primary float-right d-inline">
                <i class="fa fa-angle-double-left"></i> 
                {{ trans('messages.back') }}
            </a>
        </div>
        <div class="card-block pall-10">
            {!! Form::open(['route' => 'static-data.store', 'data-toggle' => "validator"]) !!}
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::hidden('id', isset($static->id) ? $static->id : '') !!}
                        <div class="form-group has-feedback">
                            {!! Form::label('type', trans('messages.type', ['field' => trans('messages.type')]).' *', ['class' => 'form-control-label']) !!}
                            {!! Form::text('type', isset($static->type) ? $static->type : null, ['class' => 'form-control', 'required', 'placeholder' => trans('messages.enter_field_name', ['field' => trans('messages.type')])]) !!}
                            <div class="help-block with-errors"></div>   
                            @if($errors->has('type'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif    
                        </div>
                        <div class="form-group has-feedback">
                            {!! Form::label('value', trans('messages.value', ['field' => trans('messages.value')]).' *', ['class' => 'form-control-label']) !!}
                            {!! Form::text('value', isset($static->value) ? $static->value : null, ['class' => 'form-control', 'required', 'placeholder' => trans('messages.enter_field_name', ['field' => trans('messages.value')])]) !!}
                            <div class="help-block with-errors"></div>   
                            @if($errors->has('value'))
                                <span class="text-danger">{{ $errors->first('value') }}</span>
                            @endif    
                        </div>
                        <div class="form-group has-feedback">
                            {!! Form::label('label', trans('messages.label', ['field' => trans('messages.label')]).' *', ['class' => 'form-control-label']) !!}
                            {!! Form::text('label', isset($static->label) ? $static->label : null, ['class' => 'form-control', 'required', 'placeholder' => trans('messages.enter_field_name', ['field' => trans('messages.label')])]) !!}
                            <div class="help-block with-errors"></div>
                            @if($errors->has('label'))
                                <span class="text-danger">{{ $errors->first('label') }}</span>
                            @endif    
                        </div>
                        <div class="form-group has-feedback">
                            {!! Form::label('status', trans('messages.status', ['field' => trans('messages.status')]), ['class' => 'form-control-label']) !!}
                            <br>
                            <input class="radio" type="radio" id="option1" name="status" value="0" {{ ($static->status == "0")? "checked" : "" }} >Tidak Aktif</label>
                            &nbsp;&nbsp;
                            <input class="radio" type="radio" id="option2" name="status" value="1" {{ ($static->status == "1")? "checked" : "" }} >Aktif</label>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::submit(trans('messages.save'),['class' => 'btn btn-md btn-success pull-right']) !!}
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('body_bottom')
<script>
    $(document).ready(function(){
        $('.category-list-item').addClass('active');
    });
</script>
@endsection