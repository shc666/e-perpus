@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="d-inline">Ubah Pengguna</h3>
        <a href="{{ route('users.index') }}" class="btn btn-sm btn-primary float-right d-inline">
            <i class="fa fa-angle-double-left"></i> 
            {{ trans('messages.back') }}
        </a>
    </div>
    <div class="card-block pall-30">
        {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('name', 'Nama', ['class' => 'form-control-label']) !!}
                        {!! Form::text('name', null, array('placeholder' => 'Silahkan masukkan nama', 'class' => 'form-control')) !!}
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('username', 'Username', ['class' => 'form-control-label']) !!}
                        {!! Form::text('username', null, array('placeholder' => 'Silahkan masukkan username','class' => 'form-control')) !!}
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('email', 'Email', ['class' => 'form-control-label']) !!}
                        {!! Form::text('email', null, array('placeholder' => 'Silahkan masukkan email','class' => 'form-control')) !!}
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('password', 'Password', ['class' => 'form-control-label']) !!}
                        {!! Form::password('password', array('placeholder' => 'Silahkan masukkan password','class' => 'form-control', 'id' => 'password')) !!}
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('confirm', 'Konfirmasi Password', ['class' => 'form-control-label']) !!}
                        {!! Form::password('confirm', array('placeholder' => 'Silahkan masukkan konfirmasi password','class' => 'form-control')) !!}
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('roles', 'Role', ['class' => 'form-control-label']) !!}
                        {!! Form::select('roles', $roles, [], array('class' => 'form-control')) !!}
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::submit(trans('messages.save'), ['class' => 'btn btn-md btn-success pull-right']) !!}
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection