<!-- Sidenav -->
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main"
                aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('home') }}">
            <div class="row">
                <div class="col-sm-12">
                    <img src="{{ getSingleMedia(settingSession('get'), 'site_logo', null) }}" class="navbar-brand-img" alt="site_logo">
                </div>
               <div class="col-sm-12">
                   <h1 class="text-primary m-2 mt-3"><b>{{env('APP_NAME', 'Puri Baca Bali')}}</b></h1>
               </div>
            </div>
        </a>
        {{-- <!-- User -->
        <ul class="nav align-items-center">
            <li class="nav-item dropdown">
                <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <i class="ni ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right"
                     aria-labelledby="navbar-default_dropdown_1">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="{{ getSingleMedia(\Auth::user(), 'image', null) }}">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Selamat Datang!</h6>
                    </div>
                    <a href="{{ route('admin.settings') }}" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>Pengaturan</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}" 
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                    </a>
                    {!! Form::open(['route' => 'logout', 'method'=>'POST', 'id' => 'logout-form', 'style' => 'display: none;']) !!}
                    {!! Form::close() !!}
                </div>
            </li>
        </ul> --}}
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <h1 class="text-primary m-2"><b>{{env('APP_NAME', 'Puri Baca Bali')}}</b></h1>
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                                data-target="#sidenav-collapse-main" aria-controls="sidenav-main"
                                aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended"
                           placeholder="Search" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->
            @php
                use Spatie\Menu\Link;

                $auth_user = authSession();
                if($auth_user->is('admin'))
                {
                    $menu = Menu::new([
                        Link::to(route('home'),'<i class="ni ni-tv-2 text-primary"></i> '.trans('messages.home'))->addClass('nav-link')->setActive(request()->segment(1) === 'home'),
                        Link::to(route('users.index'), '<i class="fas fa-user text-primary"></i> '.trans('messages.users'))->addClass('nav-link')->setActive(request()->segment(2) === 'users'),
                        Link::to(route('category.index'), '<i class="fas fa-list text-primary"></i> '.trans('messages.category'))->addClass('nav-link')->setActive(request()->segment(2) === 'category'),
                        Link::to(route('subcategory.index'), '<i class="fas fa-list text-primary"></i> '.trans('messages.subcategory'))->addClass('nav-link')->setActive(request()->segment(2) === 'subcategory'),
                        Link::to(route('author.index'), '<i class="fas fa-user-edit text-primary"></i> '.trans('messages.author'))->addClass('nav-link')->setActive(request()->segment(2) === 'author'),
                        Link::to(route('book.index'), '<i class="fas fa-book text-primary"></i> '.trans('messages.book'))->addClass('nav-link')->setActive(request()->is('admin/book*') ? 'active' : ''),
                        Link::to(route('book.index',['type' => 'top-sell']), '<i class="fas fa-book text-primary"></i> '.trans('messages.top_selling_book'))->addClass('nav-link'),
                        Link::to(route('book.index',['type' => 'recommended']), '<i class="fas fa-book text-primary"></i> '.trans('messages.recommended_book'))->addClass('nav-link'),
                        Link::to(route('static-data.index'), '<i class="fas fa-database text-primary"></i> '.trans('messages.static_data'))->addClass('nav-link')->setActive(request()->segment(2) === 'static-data'),
                        Link::to(route('transactions.index'), '<i class="fas fa-shopping-cart text-primary"></i> '.'Sales')->addClass('nav-link')->setActive(request()->segment(2) === 'transactions'),
                        Link::to(route('users_feedback'), '<i class="fas fa-comments text-primary"></i> '.'Users Feedback')->addClass('nav-link')->setActive(request()->segment(3) === 'feedback'),
                        Link::to(route('privacy-policy'), '<i class="fas fa-shield-alt text-primary"></i> '.'Privacy Policy')->addClass('nav-link')->setActive(request()->segment(2) === 'privacy-policy'),
                        Link::to(route('term-condition'), '<i class="fas fa-handshake text-primary"></i> '.'Term Condition')->addClass('nav-link')->setActive(request()->segment(2) === 'term-condition'),
                        Link::to(route('admin.settings'), '<i class="fas fa-cogs text-primary"></i> '.'Setting')->addClass('nav-link')->setActive(request()->segment(2) === 'settings'),
                        Link::to(route('mobileslider.index'), '<i class="fas fa-mobile-alt text-primary"></i> '.trans('messages.mobileslider'))->addClass('nav-link')->setActive(request()->segment(2) === 'mobileslider'),
                        Link::to(route('mobile_app.config'), '<i class="fas fa-mobile-alt text-primary"></i> '.'Mobile Setting')->addClass('nav-link')->setActive(request()->segment(2) === 'mobile-app'),
                    ])->addClass('navbar-nav');
                }
                if($auth_user->is('sub_admin'))
                {
                    $menu = Menu::new([
                        Link::to(route('home'),'<i class="ni ni-tv-2 text-primary"></i> '.trans('messages.home'))->addClass('nav-link')->setActive(request()->segment(1) === 'home'),
                        Link::to(route('category.index'), '<i class="fas fa-list text-primary"></i> '.trans('messages.category'))->addClass('nav-link')->setActive(request()->segment(2) === 'category'),
                        Link::to(route('subcategory.index'), '<i class="fas fa-list text-primary"></i> '.trans('messages.subcategory'))->addClass('nav-link')->setActive(request()->segment(2) === 'subcategory'),
                        Link::to(route('author.index'), '<i class="fas fa-user-edit text-primary"></i> '.trans('messages.author'))->addClass('nav-link')->setActive(request()->segment(2) === 'author'),
                        Link::to(route('book.index'), '<i class="fas fa-book text-primary"></i> '.trans('messages.book'))->addClass('nav-link')->setActive(request()->is('admin/book*') ? 'active' : ''),
                        Link::to(route('book.index',['type' => 'top-sell']), '<i class="fas fa-book text-primary"></i> '.trans('messages.top_selling_book'))->addClass('nav-link'),
                        Link::to(route('book.index',['type' => 'recommended']), '<i class="fas fa-book text-primary"></i> '.trans('messages.recommended_book'))->addClass('nav-link'),
                        Link::to(route('static-data.index'), '<i class="fas fa-database text-primary"></i> '.trans('messages.static_data'))->addClass('nav-link')->setActive(request()->segment(2) === 'static-data'),
                    ])->addClass('navbar-nav');
                }
                if($auth_user->is('user'))
                {
                    $menu = Menu::new([
                        Link::to(route('home'),'<i class="ni ni-tv-2 text-primary"></i> '.trans('messages.home'))->addClass('nav-link')->setActive(request()->segment(1) === 'home'),
                    ])->addClass('navbar-nav');
                }
            @endphp
            {!! $menu->render() !!}
        </div>
    </div>
</nav>