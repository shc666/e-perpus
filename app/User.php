<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Laravel\Passport\HasApiTokens;
use App\Category;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasMediaTrait, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name', 
        'email', 
        'password',
        'contact_number',
        'user_type',
        'image',
        'portfolio',
        'registration_id',
        'email_verified_at', 
        'device_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ratings()
    {
        return $this->hasMany(BookRating::class, 'user_id','id');
    }

    public function getCategoryData($id)
    {
        $category_data = Category::where('category_id',$id)->first();
        return $category_data;
    }

    public function user_role()
    {
        return $this->hasOneThrough('App\Role', 'App\RoleUser', 'user_id', 'id', 'id', 'role_id')->withDefault([
            'name' => ''
        ]);
    }

    public function is($roleName)
    {
        // $auth_user=authSession();
        $role=$this->user_role;
        if(isset($role))
        {
            if($role->name == $roleName)
            {
                return true;
            }
        }

        return false;
    }
}