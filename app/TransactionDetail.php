<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionDetail extends Model
{
    use SoftDeletes;

    protected $table="transactions_detail";
    protected $primaryKey='sales_detail_id';
    protected $fillable=['transaction_id','book_id' ,'user_id' ,'price' ,'discount'];

    public function getBook()
    {
        return $this->belongsTo(Book::class, 'book_id','book_id')->with(['getAuthor']);
    }

    public function getUser()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getTransaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id','transaction_id');
    }
}