<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Main admin resource controller.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
		
    }
}