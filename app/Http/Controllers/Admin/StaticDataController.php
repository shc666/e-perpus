<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\StaticData;
use DataTables;

class StaticDataController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth_user = authSession();
        if($auth_user->is('user'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $pageTitle = trans('messages.list_form_title', ['form' => trans('messages.static_data')]);
        
        return view('Admin.static_data.index', compact('pageTitle'));
    }

    public function list(Request $request)
    {
        $auth_user = authSession();
        if($auth_user->is('user'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $query = StaticData::orderBy('id', 'ASC')->get();
        return Datatables::of($query)
        ->editColumn('action', function ($query) {
            return '

            <a class="btn btn-sm btn-primary" title ="'.trans('messages.edit') .'"
                href="'.route('static-data.edit', ['id' => $query->id]).'">
              <i class="fa fa-edit"></i>
            </a>
            <a class="btn btn-sm btn-danger" 
                href="#"
                data--toggle="delete"
                data-title="All the related subcategories and books will be deleted once you confirm!"
                data-id="{{ $id }}" 
                data--url="'.route('static-data.destroy', ['id' => $query->id]).'">
              <i class="fa fa-trash"></i>
            </a>
            ';
        })
        ->addIndexColumn()
        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = -1)
    {
        $auth_user = authSession();
        if($auth_user->is('user'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        if($id != -1)
        {
            $pageTitle = trans('messages.update_form_title', ['form' => trans('messages.static_data')]);
            $static = StaticData::where('id', $id)->first();
        }
        else
        {
            $pageTitle = trans('messages.add_button_form', ['form' => trans('messages.static_data')]);
            $static = new StaticData;
        }
        
        return view('Admin.static_data.create', compact('static', 'pageTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $auth_user = authSession();
        if($auth_user->is('user'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $validator = Validator::make($request->all(),[
            'label' => 'required|regex:/^[a-z0-9 .\-]+$/i|min:2',
            'type'  => 'required|regex:/^[a-z0-9 .\-]+$/i|min:2',
            'value' => 'required|regex:/^[a-z0-9 .\-]+$/i|min:2',
        ]);
        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        $data = $request->all();
        $result = StaticData::updateOrCreate(['id' => $request->id], $data);
        $message = trans('messages.update_form', ['form' => trans('messages.static_data')]);
        if($result->wasRecentlyCreated)
        {
            $message = trans('messages.save_form', ['form' => trans('messages.static_data')]);
        }

        return redirect(route('static-data.index'))->withSuccess($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $auth_user = authSession();
        if($auth_user->is('user'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $data = StaticData::findOrFail($id);
        $data->delete();

        return response()->json([
            'status'  => true,
            'message' => trans('messages.delete_success_msg', ['form' => trans('messages.static-data') ])
        ]);
    }
}