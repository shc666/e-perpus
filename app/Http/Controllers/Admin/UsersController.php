<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\TransactionDetail;
use App\Feedback;
use App\Transaction;
use App\User;
use App\BookRating;
use App\Role;
use App\RoleUser;
use Datatables;
use Validator;
use Auth;
use Hash;

class UsersController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth_user = authSession();
        if($auth_user->is('user') || $auth_user->is('sub_admin'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $pageTitle = trans('messages.list_form_title', ['form' => trans('messages.user')]);

        return view('Admin.users.index',compact('pageTitle'));
    }

    function list()
    {
        $auth_user = authSession();
        if($auth_user->is('user') || $auth_user->is('sub_admin'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $user = User::where('user_type', 'user')->orWhere('user_type', 'sub_admin');
        return Datatables::eloquent($user)
        ->editColumn('action', function ($query) {
            return '
        
            <a class="btn btn-sm btn-primary" title="'. trans('messages.edit') .'"
                href="'.route('users.edit', $query->id).'">
                <i class="fa fa-edit "></i>
            </a>
            <a class="btn btn-sm btn-danger" 
                href="#"
                data--toggle="delete"
                data-title="All the related books will be deleted once you confirm!"
                data-id="{{ $id }}"
                data--url="'.route('user.delete', ['id' => $query->id]).'">
                <i class="fa fa-trash"></i>
            </a>';
        })
        ->addIndexColumn()
        ->rawColumns(['action'])
        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $auth_user = authSession();
        if($auth_user->is('user') || $auth_user->is('sub_admin'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $roles = Role::whereNotIn('id', [1])->pluck('name', 'id')->all();

        return view('Admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $auth_user = authSession();
        if($auth_user->is('user') || $auth_user->is('sub_admin'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $this->validate($request, [
            'name'      => 'required',
            'username'  => 'required|unique:users,username',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|same:confirm',
            'roles'     => 'required'
        ]);

        $input = $request->all();
        $get_role = Role::find($input['roles']);

        $user = new User;
        $user->name              = $input['name'];
        $user->username          = $input['username'];
        $user->email             = $input['email'];
        $user->password          = Hash::make($input['password']);
        $user->email_verified_at = Carbon::now()->toDateTimeString();
        $user->user_type         = $get_role->name;
        $user->save();

        $user_id = $user->id;
        $user_role = $get_role->id;

        $role = RoleUser::create([
            'user_id' => $user_id,
            'role_id' => $user_role
        ]);
        
        return redirect()->route('users.index')
        ->withSuccess('Pengguna berhasil dibuat!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $auth_user = authSession();
        if($auth_user->is('user') || $auth_user->is('sub_admin'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $user = User::find($id);
        $roles = Role::whereNotIn('id', [1])->pluck('name', 'id')->all();

        return view('Admin.users.edit', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $auth_user = authSession();
        if($auth_user->is('user') || $auth_user->is('sub_admin'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }
        
        $this->validate($request, [
            'name'      => 'required',
            'username'  => 'required',
            'email'     => 'required|email|unique:users,email,'.$id,
            'password'  => 'same:confirm',
            'roles'     => 'required'
        ]);

        $input = $request->all();
        $get_role = Role::find($input['roles']);
    
        $user = User::find($id);
        if(!empty($input['password']))
        {
            $user->password = Hash::make($input['password']);
        }
        else
        {
            $input = array_except($input, array('password'));
        }
        $user->name              = $input['name'];
        $user->username          = $input['username'];
        $user->email             = $input['email'];
        $user->email_verified_at = Carbon::now()->toDateTimeString();
        $user->user_type         = $get_role->name;
        $user->save();
        
        $user_id = $user->id;
        $user_role = $get_role->id;

        $role = RoleUser::where('user_id', $user_id);
        $role->update([
            'user_id' => $user_id,
            'role_id' => $user_role
        ]);

        return redirect()->route('users.index')
        ->withSuccess('Pengguna berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $auth_user = authSession();
        if($auth_user->is('user') || $auth_user->is('sub_admin'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $user = User::find($id);
        if($user == '')
        {
            return redirect()->back()->withErrors(trans('messages.check_delete_msg',['form' => trans('messages.user') ]));
        }
        Transaction::where('user_id',$user->id)->delete();
        TransactionDetail::where('user_id',$user->id)->delete();
        $user->delete();

        return redirect(route('users.index'))->withSuccess(trans('messages.delete_success_msg', ['form' => trans('messages.user') ]));
    }

    function userFeedback()
    {
        $auth_user = authSession();
        if($auth_user->is('user') || $auth_user->is('sub_admin'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $pageTitle = trans('messages.list_form_title',['form' => trans('messages.users_feedback')  ]);
        
        return view('Admin.users.users-feedback',compact('pageTitle'));
    }

    function userFeedbackDataList()
    {
        $auth_user = authSession();
        if($auth_user->is('user') || $auth_user->is('sub_admin'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }

        $user_feedback_data =  Feedback::orderBy("id",'Desc');

        return Datatables::eloquent($user_feedback_data)
        ->editColumn('name', function ($user_feedback_data) {
            return optional($user_feedback_data)->name;
        })
        ->editColumn('email', function ($user_feedback_data) {
            return optional($user_feedback_data)->email;
        })
        ->editColumn('comment', function ($user_feedback_data) {
            return '<a class="tooltip"><b>'.optional($user_feedback_data)->comment.'</b><span class="tooltip-content"><span class="tooltip-text"><span class="tooltip-inner">'.optional($user_feedback_data)->comment.'</span></span></span></a>';
        })

        ->addIndexColumn()
        ->rawColumns(['image','transaction','comment'])
        ->toJson();
    }

    public function passwordUpdate(Request $request)
    {
        $role = 'user.settings';
        
        $auth_user = authSession();
        if($auth_user->is('user') || $auth_user->is('sub_admin'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }
        if($auth_user != '' && $auth_user->is('admin'))
        {
            $role = 'admin.settings';
        }

        $user = User::find($auth_user->id);
        $page = 'password_form';

        $validator = Validator::make($request->all(), [
            'old'               => 'required|max:255',
            'password'          => 'required|min:6|confirmed|max:255',
        ],['old.*'              => 'The old password field is required.',
            'password.required' => 'The new password field is required.',
            'password.confirmed'=> "The password confirmation does not match."]);

        if($validator->fails())
        {
            return redirect()->route($role, ['page' => $page])->withErrors($validator->getMessageBag()->toArray());
        }

        $hashedPassword = $user->password;
        $match = Hash::check($request->old, $hashedPassword);
        $same_exits = Hash::check($request->password, $hashedPassword);

        if($match)
        {
            if($same_exits)
            {
                return redirect()->route($role, ['page' => $page])->withErrors(trans('messages.old_password_same'));
            }
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            Auth::logout();

            return redirect()->route($role, ['page' => $page])->withSuccess('Your password has been changed.');
        }
        else
        {
            return redirect()->route($role, ['page' => $page])->withSuccess('Please check your old password.');
        }
    }

    public function userUpdate(Request $request)
    {
        $id = $request->id;
        $page = 'profile_form';
        $role = 'user.settings';

        $auth_user = authSession();
        if($auth_user->is('user'))
        {
            return redirect()->route('home')->withSuccess(trans('messages.user_permission_denied'));
        }
        if($auth_user->is('admin') || $auth_user->is('sub_admin'))
        {
            $role = 'admin.settings';
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
            ]);
            if($validator->fails())
            {
                return redirect()->route($role, ['page' => $page])->withErrors($validator->getMessageBag()->toArray());
            }
        }

        $validator = Validator::make($request->all(), [
            'name'               => 'required|regex:/^[\pL\s-]+$/u|max:255',
            'contact_number'     => 'required|digits_between:10,12|unique:users,contact_number,'.$id,
            'profile_image'      => 'mimetypes:image/jpeg,image/png,image/jpg,image/gif|max:255',
            ],['profile_image'   => 'Image should be png/PNG, jpg/JPG',
        ]);

        if($validator->fails())
        {
            return redirect()->route($role, ['page' => $page])->withErrors($validator->getMessageBag()->toArray());
        }

        $data = $request->all();

        $result = User::updateOrCreate(['id' => $id], $data);

        if($request->profile_image != '')
        {
            uploadImage($request->profile_image,'/uploads/profile-image',$id,'image');
        }
        authSession(true);

        return redirect()->route($role, ['page' => $page])->withSuccess(trans('messages.profile').' '.trans('messages.msg_updated'));
    }
}