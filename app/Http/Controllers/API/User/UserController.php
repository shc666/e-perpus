<?php
namespace App\Http\Controllers\API\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Feedback;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
public $successStatus = 200;

    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            if($user->device_id != request('device_id')){
                $user_device = \App\User::where('device_id',request('device_id'))->first();
                if($user_device != null){
                    $user_device->registration_id = null;
                    $user_device->device_id = null;
                    $user_device->save();
                }
            }
            if(request('registration_id') != null){
                $user->registration_id = request('registration_id');
            }
            $user->device_id = request('device_id');
            $user->save();
            $success = $user;
            $success['api_token'] =  $user->createToken('MyApp')->accessToken;
            // $user->image = getSingleMedia($user,'image',null);
            $success['image'] = getBookImage($user->media,'image', $user->image,'profile-image');
            unset($success['media']);
            
            return response()->json(['data' => $success], $this->successStatus);
        }
        else{
            $message = trans('messages.auth_failed');
            return response()->json(['list' => "" ,'message'=> $message ,'status' => false], 200);
        }
    }

    public function register(Request $request){
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'regex:/^[\pL\s-]+$/u|max:255',
            'username' => 'required|unique:users,username' ,
            'email' => 'email|unique:users,email',
            'contact_number' => 'digits_between:6,12',//|unique:user_detail,contact_number',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }


            $password = $input['password'];
            $input['user_type'] = isset($input['user_type']) ? $input['user_type'] : 'user';
            $input['password'] = Hash::make($input['password']);
            $user = User::create($input);
            
            $input['api_token'] =  $user->createToken('MyApp')->accessToken;

            // Send email

            // $input['password'] = $password;
            // $to = $user->email;
            // $from = env('MAIL_USERNAME');
            // sendMail(6,$input,$to,$from);
            // $input['user_type'] = isset($input['user_type']) ? $input['user_type'] : 'user';
            // $role=\App\Role::where('name',$input['user_type'])->first();
            // if($role==""){
            //     $role=\App\Role::where('is_default',1)->first();
            // }
            $message = trans('messages.save_form',['form' => 'User']);

        return response()->json([ "status" => true , "message" => $message ,'data' => $input ]);
    }

    public function updateUserProfile(Request $request){
        $data = json_decode($request->user_detail,true);
        if ($data != "") {
            $validator = Validator::make($data, [
                'name' => 'regex:/^[\pL\s-]+$/u|max:255',
                'username' => 'required|unique:users,username,'.$data['id'] ,
                'email' => 'email|unique:users,email,'.$data['id'] ,
                'contact_number' => 'digits_between:6,12',
            ]);

            if ($validator->fails()) {
                return response()->json([ "status" => false , "errors" => $validator->getMessageBag()->all() ]);
            }
            if(isset($request->image) && $request->image != null ) {
                $data['image'] = uploadFile($request->image,'uploads/profile-image',$data['id'],'image');
            }
            $user = User::updateOrCreate(['id' => $data['id'] ], $data);
            if(isset($request->image) && $request->image != null ) {
                $user->clearMediaCollection('image');
                // $user->addMediaFromRequest('image')->toMediaCollection('image');
            }
        }
        $message = trans('messages.update_form',['form' => 'User profile']);
        $user_detail = User::where('id',optional($user)->id)->first();
        // if($user_detail->image != null) {
        //     $user_detail['image']  = fileExitsCheck(null,'/uploads/profile-image',$user_detail->image);
        // }
        $user_detail->image = getSingleMedia($user_detail,'image',null);
        return response()->json([ "status" => true , "message" => $message ,'data' => $user_detail ]);
    }

    public function saveFeedBack(Request $request){
		$validator = \Validator::make($request->all(), [
            'name'  => 'required',
            'email' => 'required|email',
            'comment' => 'required',
        ]);

        if ($validator->fails())
        {
                return \Response::json([
                    'status' => false,
                    'errors' => $validator->getMessageBag()->all()
                ]);
        }
        $temp = $request->all();
        Feedback::create($temp);

        return response()->json(['status' => true,'message' => trans('messages.save_form' ,['form' => 'Your feedback'])]);
    }

    public function logout(Request $request){
        $user = \Auth::user();
       
        if($request->is('api*')){
            $user->registration_id = null;
            $user->device_id = null;
            $user->save();
            return response()->json(['status' => true ,'message' => trans('messages.logout')]);
        }else{
            \Auth::logout();
            return redirect('/')->withSuccess('messages.logout');
        }
    }
}
