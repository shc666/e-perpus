<?php


use App\AppSetting;

function getColor(){
    $color = ['warning','danger','success','info','primary'];
    $index=array_rand($color);
    return $color[$index];
}

function setActive($path)
{
    if(!\Request::ajax()){
        return \Request::is($path . '*') ? 'active' :  '';
    }
}

function showDate($date = ''){
    if($date == '' || $date == null)
        return;

    $format = config('config.date_format') ? : 'd-m-Y';
    return date($format,strtotime($date));
}

function isSecure(){
    if(!getMode())
        return 1;

    $url = \Request::url();
    $result = strpos($url, 'wmlab');
    if($result === FALSE)
        return 0;
    else
        return 1;
}

function getRemoteIPAddress() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];

    } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    return $_SERVER['REMOTE_ADDR'];
}

function getClientIp(){
    $ips = getRemoteIPAddress();
    $ips = explode(',', $ips);
    return !empty($ips[0]) ? $ips[0] : \Request::getClientIp();
}

function sendMail($to,$from,$maildata,$mail_subject){
    $data['mail_body'] = $maildata;
    Mail::send('Admin.mail_template.mail_template',$data, function($message) use($to,$from,$maildata,$mail_subject){
         $message->to($to)->subject($mail_subject);
         $message->from($from,'Granth');
     });
}

function uploadImage($file, $path, $id, $field_name)
{

    if ($file != null) {
        $paths = public_path().$path;
        if (!file_exists($paths)) {
            File::makeDirectory($paths, $mode = 0777, true, true);
        }
    } else {
        $filename = \DB::table('users')->where('id', $id)->value($field_name);
    }

    if ($file!='') {
        $filename =time().'-'.$file->getClientOriginalName();
        $file->move($paths, $filename);

        $images[$field_name]=$filename;
        return \App\User::updateOrCreate(['id' => $id], $images);
    }
}
function uploadFile($file, $path,$id = '' , $field_name = '')
{
    if ($file != null) {
        $paths = public_path().'/'.$path.'/';
        if (!file_exists($paths)) {
            File::makeDirectory($paths, $mode = 0777, true, true);
        }
        $filename =time().'-'.$file->getClientOriginalName();
        move_uploaded_file($file,$paths.$filename);
        return $filename;
    }
    else{
        return null;
    }
}




function envChanges($type,$value){
    $path = base_path('.env');
    if (file_exists($path)) {
        file_put_contents($path, str_replace(
            $type.'='.env($type), $type.'='.$value, file_get_contents($path)
        ));
        if( in_array( $type ,['ONESIGNAL_API_KEY','ONESIGNAL_REST_API_KEY']) ){
            if(getenv($type) == false){
                file_put_contents($path,"\n".$type.'='.$value ,FILE_APPEND);
            }
        }
    }
}

function CheckRecordExist($table_list,$column_name,$id){
    $search_keyword = $column_name;
    if(count($table_list) > 0){
        foreach($table_list as $table){
            $check_data = \DB::table($table)->where('category_id',$id)->WhereNull('deleted_at')->count();
            if($check_data > 0)
            {
                return false ;
            }
        }
        return true;
    }
    else {
        return true;
    }
}

function sendOneSignalMessage($device_ids, $device_data) {
    if(env('ONESIGNAL_API_KEY') != null && env('ONESIGNAL_REST_API_KEY') != null){
        $content = array(
            "en" => isset($device_data['message']) ? $device_data['message'] :  env('APP_NAME').' Message'
        );
        $heading = array(
            "en" =>isset($device_data['title']) ? $device_data['title'] :  env('APP_NAME').' Title'
        );
        $device_contents = array(
                "type"       => isset($device_data['type']) ? $device_data['type'] :  env('APP_NAME'),
                "title"      =>  isset($device_data['title']) ? $device_data['title'] :  env('APP_NAME').' Title',
                "message"    => isset($device_data['message']) ? $device_data['message'] :  env('APP_NAME').' Message',
                "book_id"    => isset($device_data['book_id']) ? $device_data['book_id'] : 'book_id',
                "notification_type"    => isset($device_data['notification_type']) ? $device_data['notification_type'] : 'notification_type',
            );
    
        $fields = array(
            'app_id' => env('ONESIGNAL_API_KEY'),
            'include_player_ids' => $device_ids,
            'data' => $device_contents,
            'contents' => $content,
            'headings' => $heading,
            'big_picture' => isset($device_data['image']) ? $device_data['image'] : 'Image',
        );
    
        $header=[
            "title" => isset($device_data['title']) ? $device_data['title'] :  env('APP_NAME').' Title',
            "message" => isset($device_data['message']) ? $device_data['message'] :  env('APP_NAME').' Message',
        ];
    
    
    
        $sendContent = json_encode($fields);
        // dd($sendContent);
        oneSignalAPI($sendContent);
    }
}

function oneSignalAPI($sendContent)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER,
                        array('Content-Type: application/json;
                                charset=utf-8',
                                 'Authorization: Basic '.env('ONESIGNAL_REST_API_KEY')
                        ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $sendContent);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);
    // dd($response);
    return $response;
}

function getSingleMedia($model, $collection = 'image_icon',$image_type='',$file_type='image',$skip=true)
{
    switch($file_type){
        case 'video':
                    if (!\Auth::check() && $skip) {
                        return asset('assets/sample_file/sample.mp4');
                    }
                    if ($model !== null) {
                        $media = $model->getFirstMedia($collection);
                    }
                    $imgurl= isset($media)?$media->getPath():'';

                    if (file_exists($imgurl)) {
                        return $media->getFullUrl();
                    }else{
                        $media = asset('assets/sample_file/sample.mp4');
                        if($collection == 'file_sample_path'){
                            $media = fileExitsCheck($media,'/uploads/sample-file',$model->file_sample_path);
                        }
                        if($collection == 'file_path'){
                            $media = fileExitsCheck($media,'/uploads/file-path',$model->file_path);
                        }
                        return $media;
                    }
        case 'epub':
                    if (!\Auth::check() && $skip) {
                        return asset('assets/sample_file/sample.epub');
                    }
                    if ($model !== null) {
                        $media = $model->getFirstMedia($collection);
                    }
                    $imgurl= isset($media)?$media->getPath():'';

                    if (file_exists($imgurl)) {
                        return $media->getFullUrl();
                    }else{
                        $media = asset('assets/sample_file/sample.epub');
                        if($collection == 'file_sample_path'){
                            $media = fileExitsCheck($media,'/uploads/sample-file',$model->file_sample_path);
                        }
                        if($collection == 'file_path'){
                            $media = fileExitsCheck($media,'/uploads/file-path',$model->file_path);
                        }
                        return $media;
                    }
        case 'pdf':
                    if (!\Auth::check() && $skip) {
                        return asset('assets/sample_file/sample.pdf');
                    }
                    if ($model !== null) {
                        $media = $model->getFirstMedia($collection);
                    }
                    $imgurl= isset($media)?$media->getPath():'';

                    if (file_exists($imgurl)) {
                        return $media->getFullUrl();
                    }else{
                        $media = asset('assets/sample_file/sample.pdf');
                        if($collection == 'file_sample_path'){
                            $media = fileExitsCheck($media,'/uploads/sample-file',$model->file_sample_path);
                        }
                        if($collection == 'file_path'){
                            $media = fileExitsCheck($media,'/uploads/file-path',$model->file_path);
                        }
                        return $media;
                    }
        case 'image':
        default:
                if (!\Auth::check() && $skip) {
                    return asset('assets/img/icons/user/user.png');
                }
                if ($model !== null) {
                    $media = $model->getFirstMedia($collection);
                }
                if(isset($image_type)){
                    $imgurl= isset($media)?$media->getPath('thumb'):'';
                }else{
                    $imgurl= isset($media)?$media->getPath():'';
                }
                if (file_exists($imgurl)) {
                    if(isset($image_type)){
                        return $media->getFullUrl('thumb');
                    }else{
                        return $media->getFullUrl();
                    }
                }else{
                    $media ="";
                switch ($collection) {
                    case 'image_icon':
                    case 'image':
                        $default = asset('assets/img/icons/user/user.png');
                        if($media == null){
                            if($model != null){
                                $media =  getImageFile($model,$default);
                            }else{
                                $media = $default;
                            }
                        }
                        break;
                    default:
                        $default = asset('assets/img/icons/common/add.png');
                        if($media == null){
                            if($model != null){
                                $media =  getImageFile($model,$default,$collection);
                            }else{
                                $media = $default;
                            }
                        }
                        break;
                }

                return $media;

                }
    }
}

function getImageFile($model,$default,$collection = ""){
    $table = $model->getTable();
    switch($table) {
        case 'author':
                $file = fileExitsCheck($default,'/uploads/author-image',$model->image);
                break;
        case 'book':
                if($collection == 'front_cover'){
                    $file = fileExitsCheck($default,'/uploads/front-image',$model->front_cover);
                }
                if($collection == 'back_cover') {
                    $file = fileExitsCheck($default,'/uploads/back-image',$model->back_cover);
                }
                break;
        case 'app_setting':
                if($collection == 'site_logo'){
                    $file = fileExitsCheck($default,'/uploads/app',$model->site_logo);
                }
                if($collection == 'site_favicon') {
                    $file = fileExitsCheck($default,'/uploads/app',$model->site_favicon);
                }
                break;
        case 'mobile_slider':
                if($collection == 'slide_image') {
                    $file = fileExitsCheck($default,'/uploads/mobile_slider',$model->slide_image);
                }
                break;
        case 'users':
                $file = fileExitsCheck($default,'/uploads/profile-image',$model->image);
                break;
        default:
                $file = asset('assets/img/icons/common/add.png');
                break;
    }
    return $file;
}
function getBookImage($img_media,$collection,$filename="",$filetype=""){
    $image = "";
    $img = $img_media->where('collection_name',$collection)->last();
    if($img){
        $image = $img->getFullUrl();
    }else{
        // $image = fileExitsCheck(null,'/uploads/front-image',$filename);
        switch($collection){
            case 'front_cover':
                $image = fileExitsCheck(null,'/uploads/front-image',$filename);
                break;
            case 'back_cover':
                $image = fileExitsCheck(null,'/uploads/back-image',$filename);
                break;
            case 'file_path':
                $image = fileExitsCheck(null,'/uploads/file-path',$filename);
                if($image == null){
                    $image = getBookFile($filetype);
                }
                break;
            case 'file_sample_path':
                $image = fileExitsCheck(null,'/uploads/sample-file',$filename);
                if($image == null){
                    $image = getBookFile($filetype);
                }
                break;
            case 'image':
                $image = asset('assets/img/icons/common/add.png');

                if($filetype == 'author-image'){
                    $image = fileExitsCheck(null,'/uploads/author-image',$filename);
                }
                if($filetype == 'profile-image'){
                    $image = fileExitsCheck(null,'/uploads/profile-image',$filename);
                }
                break;
            case 'slide_image':
                $image = asset('assets/img/icons/common/add.png');
                if($filetype == 'mobile_slider'){
                    $image = fileExitsCheck(null,'/uploads/mobile_slider',$filename);
                }
            break;
            default:
                $image = asset('assets/img/icons/common/add.png');
                break;
        }
        return $image;
    }
    return $image;
}
function getBookFile($filetype){
    $file = asset('assets/sample_file/sample.pdf');
    
    if( $filetype != null  ){
        switch($filetype){
            case 'video':
                    $file = asset('assets/sample_file/sample.mp4');
                    break;
            case 'epub':
                    $file = asset('assets/sample_file/sample.epub');
                    break;
            case 'pdf':
                    $file = asset('assets/sample_file/sample.pdf');
                    break;
            default :
                return $file;
                break;
        }
    }
    return $file;
}
// function getBookImage($filename,$folder_name,$filetype= null){
//     $file = fileExitsCheck(null,'/uploads/'.$folder_name,$filename);
//     if(!isset($file)){
//         if( $filetype != null && in_array($folder_name,['file-path','sample-file']) ){
//             switch($filetype){
//                 case 'video':
//                         $file = asset('assets/sample_file/sample.mp4');
//                         break;
//                 case 'epub':
//                         $file = asset('assets/sample_file/sample.epub');
//                         break;
//                 case 'pdf':
//                         $file = asset('assets/sample_file/sample.pdf');
//                         break;
//                 default :
//                     return $file;
//                     break;
//             }
//         }    
//     }   
//     return $file;
// }
function authSession($force=false){
    $session=new \App\User;
    if($force){
        $user=\Auth::user(); //->with('user_role');
        \Session::put('auth_user',$user);
        $session =\Session::get('auth_user');
        return $session;
    }
    if(\Session::has('auth_user')){
        $session =\Session::get('auth_user');
    }else{
        $user=\Auth::user();
        \Session::put('auth_user',$user);
        $session =\Session::get('auth_user');
    }
    return $session;
}

function settingSession($type='get'){
    if(\Session::get('setting_data')==''){
        $type='set';
    }
    switch ($type){
        case "set" : $settings = AppSetting::first();\Session::put('setting_data',$settings); break;
        default : break;
    }
    return \Session::get('setting_data');
}

function money($price,$symbol='$'){
    if($price==''){
        $price=0;
    }
    return $symbol.' '.$price;
}

function fileExitsCheck($defaultimg, $path, $filename)
{
    $image= $defaultimg;
    $imgurl= public_path($path.'/'.$filename);
    if ($filename != null && file_exists($imgurl)) {
        $isimgurl=URL::asset($path.'/'.$filename);
        $image=$isimgurl;
    }
    return $image;
}

?>
