## Vanguard - Advanced PHP Login and User Management

## Backend and API Crafted with Laravel 7

- Website: https://vanguardapp.io
- Documentation: https://milos.support-hub.io
- Developed by [Chandrayana Putra]